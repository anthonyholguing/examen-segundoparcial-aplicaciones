//REGISTRO DE DATOS DE CLIENTE
const dbConnection = require('./connection')

module.exports = app => {
    const connection = dbConnection

    function guardarDatos() {
    let cedula = document.getElementById("cedula").value;
    let nombres = document.getElementById("nombres").value;
    let direccion = document.getElementById("direccion").value;
    let telefono = document.getElementById("telefono").value;
    let correo_electronico = document.getElementById("correo_electronico").value;
    //Proceso de registro
    app.post('/register', (req, res) => {
      
        const {
            cedula,
            nombres,
            direccion,
            telefono,
            correo_electronico
       
        }
        //Se realiza la inserccion en la base de datos 
        connection.query('INSERT INTO usuario SET ?', {
            cedula, nombres, direccion,telefono, correo_electronico
        }, (err, result) => {
            //Una ves guardado en el caso deberia direccionarnos al login
            res.redirect('/login')
        })
    })
}
}