//CONEXION A LA BASE DE DATOS
const mysql = require('mysql')

const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'database'
})

connection.connect(function(err){
    /*Si existe algun error en la conexion*/
    if(err) {
        console.log("Error al conectar con la base de datos")
    /*Si no existe algun error*/

    } else {
        console.log("Conexión correcta a la base de datos")
    }
})
//Exportar del modulo para usarlo en el registro
module.exports = connection